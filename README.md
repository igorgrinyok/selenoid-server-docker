# Selenoid and Allure Report Server Docker

___

### Two-command Installation

``` 
cp .env.example env.py
docker-compose up --build 
```

### The project consists of 2 libraries

1. [Selenoid](https://github.com/aerokube/selenoid)
2. [Allure-Server](https://github.com/kochetkov-ma/allure-server)

___

## Docker

env.py for docker

```
cp .env.example env.py
```

Build and run images

```
docker-compose up --build 
```

___

## Selenoid

Selenoid requires browser settings and installed browser images.

### Example:

#### browsers.json:

```
  "chrome": {
    "default": "latest",
    "versions": {
      "latest": {
        "image": "selenoid/chrome",
        "port": "4444",
        "path": "/",
        "tmpfs": {
          "/tmp": "size=128m"
        },
        "enableVNC": "true"
      }
    }
  }
```

#### Required image:

``` 
docker pull selenoid/chrome
 ```

___
